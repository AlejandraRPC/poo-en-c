#include <stdio.h>

typedef struct Cancion{
    char titulo [41];
    char artista[41];
    char tiempo[10];

}Cancion_t;


Cancion_t crear_cancion(){
    
    Cancion_t nuevo_cancion;
    
    printf("\nIngrese el nombre de la cancion: ");
    fflush(stdin);
    gets(nuevo_cancion.titulo);

    
    printf("\nIngrese el artista o banda de la cancion: ");
    fflush(stdin);
    gets(nuevo_cancion.artista);

   
    printf("\nIngrese la duracion de la cancion (mm:ss): ");
    fflush(stdin);
    gets(nuevo_cancion.tiempo);
    
    printf("\n");

    
    return nuevo_cancion;

}

void imp_cancion(Cancion_t Cancion){
	//printf("%s",cancion.titulo);
	//printf("\n");
    printf("Cancion: %s , Artista o banda: %s , Duracion: %s minutos\n",Cancion.titulo,Cancion.artista,Cancion.tiempo);
}

int main(void){
    
    Cancion_t v1 = crear_cancion();
    Cancion_t v2 = crear_cancion();

    
    imp_cancion(v2);
    imp_cancion(v1);

    return 0;
}
